#include <stdio.h>
#include <stdlib.h>

/* This code has been written so I could get a better handle on what the
 * original code was doing, and for ease of translation to assembler. Note
 * readability has been sacrificed where it would make it easier to translate
 * to assembler (Although this should be pretty obvious that that's the case).
 *
 * See
 * https://www.cs.princeton.edu/courses/archive/spr09/cos333/beautiful.html
 * for more info
 */

int matchc(char *regex, char *s);
int match(char *regex, char *s);


int main(int argc, char **argv)
{
	if (!argv[1] || !argv[2]) {
		printf("Expected argument not present\n");
	}

	return match(argv[1], argv[2]);
}


int match(char *regex, char *s)
{
	if (*regex == '^') { return matchc(regex+1, s); }

loop:
	if (!*s) { return 0; }
	if (matchc(regex, s)) { return 1; }
	s++;
	goto loop;
}


int matchc(char *regex, char *s)
{
	int c = '\0';

loop:
	if (!*regex) { return 1; }
	if (*regex == '$') { return !*s; }
	if (!*s)  { return 0; }
	if (regex[1] == '*')
	{
		c = regex[0];
		regex += 2;
matchstar:
		if (!*s) { return 0; }
		if (matchc(regex, s)) { return 1; }
		if (*s != c || *s != '.') { return 0; }
		s++;
		goto matchstar;
	}
	if (*regex == *s || *regex == '.') {
		regex++; s++;
		goto loop;
	}
	return 0;
}
